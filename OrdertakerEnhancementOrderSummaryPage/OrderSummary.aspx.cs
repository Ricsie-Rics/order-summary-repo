﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
//Additional Namespace
using System.Data.SqlClient;
using System.Data;

namespace Hierarchal_GridView_2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public class MyClass
        {
            public string Urlpic { get; set; }
            public string NickName { get; set; }
            public int OrderTotal { get; set; }
        }

        public class Order {
            public string OrderUser { get; set; }
            public string OrderName { get; set; }
            public int OrderQuantity { get; set; }
            public int OrderPrice { get; set; }            
        }

        public class OrderTotalperUser
        {
            public string OrderUser { get; set; }
            public string Urlpic { get; set; }
            public string NickName { get; set; }
            public int OrderTotal { get; set; }
        }


        List<Order> OrderCollection = new List<Order>();
        List<Order> myOrderCollection2 = new List<Order>();

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isHttpRequestLocal = GetHttpRequestIfLocal();
            BindChildGridView();

            //string ordertakerConnectionString = ConfigurationManager.ConnectionStrings["OrdertakerConnectionString"].ToString();
            string ordertakerConnectionString; //= ConfigurationManager.ConnectionStrings["test"].ToString();
            if (isHttpRequestLocal)
            {
                ordertakerConnectionString = ConfigurationManager.ConnectionStrings["test"].ToString();
            }
            else
            {
                ordertakerConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ToString();
            }
            SqlConnection myConnection = new SqlConnection(ordertakerConnectionString);
            SqlCommand myCommand = new SqlCommand("[dbo].[sp_getorderTotalperUserfortheday2]", myConnection);
            myConnection.Open();
            myCommand.CommandType = CommandType.StoredProcedure;
            SqlDataReader myReader = myCommand.ExecuteReader();
            List<OrderTotalperUser> myOrderTotalperUserCollection = new List<OrderTotalperUser>();
            while (myReader.Read())
            {
                OrderTotalperUser myOrderTotalperUser = new OrderTotalperUser();
                myOrderTotalperUser.OrderUser = myReader["orderUser"].ToString();
                if (isHttpRequestLocal)
                {
                    myOrderTotalperUser.Urlpic = "http://lpd-0250/pics/" + myReader["employeeNumber"].ToString() + ".jpg";
                }
                else
                {
                    myOrderTotalperUser.Urlpic = "~/Images/orderUserPics/" + myReader["employeeNumber"].ToString() + ".jpg";
                }
                myOrderTotalperUser.NickName = myReader["nickName"].ToString();
                myOrderTotalperUser.OrderTotal = Convert.ToInt32(myReader["orderTotal"]);
                myOrderTotalperUserCollection.Add(myOrderTotalperUser);
            }
            myConnection.Close();

            //List<MyClass> testCollection = new List<MyClass>();

            //MyClass test = new MyClass();
            //test.Urlpic = "http://haunter/pics/130280.jpg";
            //test.NickName = "Elvin";
            //test.OrderTotal = 88;

            //MyClass test2 = new MyClass();
            //test2.Urlpic = "http://haunter/pics/130653.jpg";
            //test2.NickName = "Marj";
            //test2.OrderTotal = 49;

            //MyClass test3 = new MyClass();
            //test3.Urlpic = "http://haunter/pics/120354.jpg";
            //test3.NickName = "Kathleen";
            //test3.OrderTotal = 7;

            //testCollection.Add(test);
            //testCollection.Add(test2);
            //testCollection.Add(test3);

            //Order myOrder = new Order();
            //myOrder.OrderName = "Pusit";
            //myOrder.OrderPrice = 35;
            //myOrder.OrderUser = "Elvin";

            //Order myOrder2 = new Order();
            //myOrder2.OrderName = "Chicken";
            //myOrder2.OrderPrice = 59;
            //myOrder2.OrderUser = "Marj";

            //Order myOrder3 = new Order();
            //myOrder3.OrderName = "Rice";
            //myOrder3.OrderPrice = 7;
            //myOrder3.OrderUser = "Kathleen";

            //OrderCollection.Add(myOrder);
            //OrderCollection.Add(myOrder2);
            //OrderCollection.Add(myOrder3);

            //GridView1.DataSource = testCollection;
            GridView1.DataSource = myOrderTotalperUserCollection;
            GridView1.DataBind();

            //http://haunter/pics/048100.jpg
            //http://haunter/pics/064279.jpg

            
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //string OrderUser = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
                //GridView GridView2 = e.Row.FindControl("GridView2") as GridView;
                //GridView2.DataSource = OrderCollection;
                //GridView2.DataBind();
                
                //string customerID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "NickName"));
                //List<Order> filteredOrderCollection = new List<Order>();
                //foreach (Order eachOrder in OrderCollection) {
                //    if (eachOrder.OrderUser.Equals(customerID)) {
                //        filteredOrderCollection.Add(eachOrder);
                //    }
                //}

                string customerID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "orderUser"));
                List<Order> filteredOrderCollection = new List<Order>();
                foreach (Order eachOrder in myOrderCollection2)
                {
                    if (eachOrder.OrderUser.Equals(customerID))
                    {
                        filteredOrderCollection.Add(eachOrder);
                    }
                }

                GridView gvChild = (GridView)e.Row.FindControl("nestedGridView");
                //SqlDataSource gvChildSource = new SqlDataSource();
                //gvChildSource.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                //gvChildSource.SelectCommand = "SELECT [OrderID], [OrderDate],[Freight] FROM [Orders] WHERE [CustomerID] = '" + customerID + "'";
                gvChild.DataSource = filteredOrderCollection;
                gvChild.DataBind();
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                //e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");


                e.Row.Attributes["onmouseover"] = "javascript:setMouseOverColor(this);";
                e.Row.Attributes["onmouseout"] = "javascript:setMouseOutColor(this);";
                //e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.GridView1, "Select$" + e.Row.RowIndex);
            }
        }

        public void BindChildGridView()
        {
            //"Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;
            //string ordertakerConnectionString = ConfigurationManager.ConnectionStrings["OrdertakerConnectionString"].ToString();
            string ordertakerConnectionString;// = ConfigurationManager.ConnectionStrings["test"].ToString();
            if (HttpContext.Current.Request.IsLocal)
            {
                ordertakerConnectionString = ConfigurationManager.ConnectionStrings["test"].ToString();
            }
            else
            {
                ordertakerConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ToString();
            }
            SqlConnection myConnection = new SqlConnection(ordertakerConnectionString);
            SqlCommand myCommand = new SqlCommand("[dbo].[sp_getglobalorders2]", myConnection);
            myConnection.Open();
            myCommand.CommandType = CommandType.StoredProcedure;
            SqlDataReader myReader =  myCommand.ExecuteReader();
            //List<Order> myOrderCollection2 = new List<Order>();
            while (myReader.Read())
            {
                Order myOrder2 = new Order();
                myOrder2.OrderUser = myReader["orderUser"].ToString();
                myOrder2.OrderName = myReader["orderName"].ToString();
                myOrder2.OrderQuantity = Convert.ToInt32(myReader["orderQuantity"]);
                myOrder2.OrderPrice = Convert.ToInt32(myReader["orderPrice"]);
                myOrderCollection2.Add(myOrder2);
            }
            myConnection.Close();
        }

        private bool GetHttpRequestIfLocal()
        {
            if (HttpContext.Current.Request.IsLocal)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}