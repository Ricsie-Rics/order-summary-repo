﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderSummary.aspx.cs" Inherits="Hierarchal_GridView_2.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/StyleSheet1.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 32px;
            height: 32px;
        }
    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "Images/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "Images/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>

    <%--<script type="text/javascript">
        $("[Id*=pnlOrders]").live("click", function () {
            $("[src*=plus]").closest("tr").after("<tr><td></td><td colspan = '999'>" + $("[src*=plus]").next().html() + "</td></tr>");
        });
    </script>--%>

    <script type = "text/javascript">
         function MouseEvents(objRef, evt) {
             var checkbox = objRef.getElementsByTagName("input")[0];
             if (evt.type == "mouseover") {
                 objRef.style.backgroundColor = "orange";
             }
             else if (evt.type == "mouseout") {
                if (objRef.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    objRef.style.backgroundColor = "#C2D69B";
                }
                else {
                    objRef.style.backgroundColor = "white";
                }
            }
            
         }
    </script>

    <script language="javascript" type="text/javascript">
        var oldgridSelectedColor;

        function setMouseOverColor(element) {
            oldgridSelectedColor = element.style.backgroundColor;
            element.style.backgroundColor = 'yellow';
            element.style.cursor = 'hand';
            element.style.textDecoration = 'underline';
        }

        function setMouseOutColor(element) {
            element.style.backgroundColor = oldgridSelectedColor;
            element.style.textDecoration = 'none';
        }
    </script>
    
    <script type="text/javascript">
        function collapseExpand(obj) {
            var gvObject = document.getElementById(obj);
            var imageID = document.getElementById('image' + obj);
            if (gvObject.style.display == "none") {
            //alert('alert ' + gvObject.title);
            //if (gvObject.title == "test") {
                gvObject.style.display = "inline";
                imageID.src = "Images/minus.png";
                //$(obj).closest("tr").after("<tr><td></td><td colspan = '999'>" + $("[src*=plus]").next().html() + "</td></tr>")
                //$("[src*=plus]").closest("tr").after("<tr><td></td><td colspan = '999'>" + $("[src*=plus]").next().html() + "</td></tr>")
                //$("[src*=plus]").attr("src", "Images/minus.png");
                //alert('call1');
                //gvObject.title = "in";
                //alert('call1');
            }
            else {
                gvObject.style.display = "none";
                imageID.src = "Images/plus.png";
                //$("[src*=minus]").attr("src", "Images/plus.png");
                //$("[src*=minus]").closest("tr").next().remove();
                //alert('call2');
                //gvObject.title = "test";
            }
        }
    </script>

    

    <script type="text/javascript">
        function GetSelectedRow(lnk) {
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;

            var gridViewCtlId = '<%=GridView1.ClientID%>';
            var grid = document.getElementById(gridViewCtlId);
            var column = grid.rows[rowIndex + 1].cells;
            if (column[3].innerHTML != "<img src=Images/stamp-received.png height=60px width=80px OnClientClick=return GetSelectedRow(this); javascript:shouldsubmit=true;></img>") {
                column[3].innerHTML = "<img src=Images/stamp-received.png height=60px width=80px OnClientClick=return GetSelectedRow(this); javascript:shouldsubmit=true;></img>";
            }
            else {
                column[3].innerHTML = "<img src=Images/erroricon.png height=60px width=80px></img>";
            }

            return false;
        }
    </script>

    <script type="text/javascript">
        function GetIndex(obj) {
            var row = obj.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var test = obj.src.valueOf()

            if (obj.src.match("stamp-received")) {
                obj.src="Images/erroricon.png"
            }
            else {
                obj.src = "Images/stamp-received.png";
            }
            return false;
        }
    </script>   

</head>

<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="orderUser" onrowdatabound="GridView1_RowDataBound"
            Font-Size = "11pt" AlternatingRowStyle-BackColor = "#C2D69B" 
            GridLines="Horizontal" Width="300px">
            <Columns>
                <%--<asp:TemplateField HeaderStyle-CssClass="demo-pricing-1">
                    <ItemTemplate>
                        <img alt="" class="style1" src="Images/plus.png" />
                         <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid" GridLines="Vertical">
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="100px" DataField="OrderName" HeaderText="Order Name" />
                                    <asp:BoundField ItemStyle-Width="100px" DataField="OrderPrice" HeaderText="Order Price" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>--%>
               <%-- <asp:TemplateField HeaderText="Ex" HeaderStyle-CssClass="demo-pricing-1" >
                    <ItemTemplate>
                    
                         <img alt="" class="style1" src="Images/plus.png" />
                        <tr><td>
                        <div id="customerID-<%# Eval("NickName") %>" 
                             style="display:inline;
                             position:relative;left:25px;" title="test">
         
                            <asp:GridView ID="nestedGridView" runat="server" 
                                          AutoGenerateColumns="False" 
                                          DataKeyNames="OrderUser" >
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="100px" DataField="OrderName" HeaderText="Order Name" />
                                    <asp:BoundField ItemStyle-Width="100px" DataField="OrderPrice" HeaderText="Order Price" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        </tr></td>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="demo-pricing-1" >
                    <ItemTemplate>
                         <a href="javascript:collapseExpand('customerID-<%# Eval("orderUser") %>');">
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Urlpic") %>' Width="100px" Height="100px" />
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-CssClass="demo-pricing-1" HeaderText="Names">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("NickName") %>' ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField='OrderTotal' HeaderText="Total" HeaderStyle-CssClass="demo-pricing-1" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="myCssClass" />
               

                 <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="demo-pricing-1">
                    <ItemTemplate>
                        <input id="myButton" type="image" class="btnClaim" src="Images/erroricon.png" onclick="return GetIndex(this);" height="60px" width="80px">
                    </ItemTemplate>
                 </asp:TemplateField>

                <asp:TemplateField>
                <ItemTemplate>
                    <tr><td colspan="100%">
                        <div id="customerID-<%# Eval("orderUser") %>" 
                             style="display:none;
                             position:relative;left:25px;">
         
                            <asp:GridView ID="nestedGridView" runat="server" 
                                          AutoGenerateColumns="False" 
                                          DataKeyNames="OrderUser">
                                <Columns>
                                    <asp:BoundField ItemStyle-Width="150px" DataField="OrderName" HeaderText="Order Name" />
                                    <asp:BoundField ItemStyle-Width="23px" DataField="OrderQuantity" HeaderText="Qty" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField ItemStyle-Width="50px" DataField="OrderPrice" HeaderText="Price" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td></tr>
                </ItemTemplate>
            </asp:TemplateField>

            </Columns>
        </asp:GridView>
    
    
    </div>
    </form>
</body>
</html>
